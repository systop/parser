require_relative 'lib/loger.rb'
require_relative 'lib/class_twitter.rb'
require_relative 'lib/class_telegram.rb'
require_relative 'lib/class_watched_tweets.rb'
#require 'twitter'
require 'pry'
require 'pry-byebug'
#require 'telegram/bot'
#require 'yaml'

include Loger

def main_cycle
  #vars
  config_path = File.join(File.dirname(__FILE__), 'warehouse/config.yml')
  #config_path = './warehouse/config.yml'
  watched_tweets_path = File.join(File.dirname(__FILE__), 'warehouse/watched_tweets.yml')
  #watched_tweets_path = './warehouse/watched_tweets.yml'

  #check config file
  if File.exist?(config_path)
    log 'Config file found'
    config = YAML::load_file(config_path)
  else
    log 'Config NOT file found'
    raise "Config file \"#{config_path.to_s}\" not found"
  end

  #create twitter client
  tw_api = Twitter_api.new config[:twitter]

  #create telegram client
  #telegram_api = Telegram_api.new config[:telegram_bot]
  telegram_api = Telegram_api.new config

  #create watched tweets writer
  watched_tweets = ClassWatchedTweets.new watched_tweets_path

  #WORK
  config[:people].each do |human|
    #parse tweets - returns ARRAY of tweets with accordance likes count
    parsed = tw_api.parse_tweets human
    #send to chat
    parsed.each do |tweet|
      #check if watched
      unless watched_tweets.watched?(tweet[:url])
        telegram_api.send_message "#{tweet[:likes]} likes tweet from #{tweet[:name]}\nLink: #{tweet[:url]}"
        watched_tweets.tweet_to_watched(tweet[:url]) #write to file
      end
    end
  end

  watched_tweets.write_file

  telegram_api.listen({tw_api: tw_api, config_path: config_path})

  sleep config[:main][:interval]
rescue
  log 'FATAL ERROR'
  #telegram_api.send_message "FATAL ERROR - что то сломалось"
end

Thread.abort_on_exception = true

thread1 = Thread.new { loop { main_cycle } }.join


#binding.pry



puts 'script finished'