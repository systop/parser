require 'date'

#writes logs to 'warehouse/log.txt' with date

module Loger

  def log(text)
    #get config
    config_path = File.join(File.dirname(__FILE__), '../warehouse/config.yml')
    config = YAML::load_file(config_path)

    log_path = File.join(File.dirname(__FILE__), '../warehouse/log.txt')

    if File.exist?(log_path)
      log = File.open(log_path, 'r').read
    else
      log = File.new(log_path, "w")#if new - error '+' on object
    end
    log += "\n#{DateTime.now.to_s} "
    log += "#{text}"

    log_length = log.split("\n").length
    config_length = config[:main][:log_length_max]
    if log_length > config_length
      log = log.split("\n")[-config_length..log_length].join("\n")
    end

    File.open(log_path, 'w') {|file| file.write log}

  end

end