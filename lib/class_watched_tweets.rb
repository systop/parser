#class_watched_tweets.rb
require 'yaml'

class ClassWatchedTweets

  def initialize(path)#first time came nil
    #get watched tweets
    if File.exist?(path)
      @watched_tweets = YAML::load_file(path)
      @watched_old = @watched_tweets
      @path = path
    else
      empty = ['start_record']
      File.open(path, 'w') {|f| f.write empty.to_yaml }
      @watched_tweets = []
    end
  end

  def watched?(url)
    if @watched_tweets.include? url
      true
    else
      false
    end
  end

  def tweet_to_watched(url)
    @watched_tweets << url
  end

  def write_file
    #if @watched_tweets != @watched_old
      File.open(@path, 'w') {|f| f.write @watched_tweets.to_yaml }
    #end
  end

end