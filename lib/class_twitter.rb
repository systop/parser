require_relative 'loger.rb'
require 'twitter'
require 'pry'
require 'pry-byebug'

class Twitter_api

  def initialize(config)
    @config = config
    create_client(@config)
  end

  def create_client(config)
    @client = Twitter::REST::Client.new do |tw_config|
      tw_config.consumer_key        = config[:consumer_key]
      tw_config.consumer_secret     = config[:consumer_secret]
      tw_config.access_token        = config[:access_token]
      tw_config.access_token_secret = config[:access_token_secret]
    end
    log 'Twitter class created client'
  end

  def parse_tweets(hash)
    #initialize returning array
    parsed_array = []
    #parse
    ###puts "--------#{hash[:screen_name]}--------"
    ###puts hash
    tweets_array = @client.user_timeline(hash[:screen_name])
    tweets_array.each do |tweet|
      #raise 'EASY'
      text = tweet.text
      likes = tweet.favorite_count
      url = tweet.url.to_s
      if likes > hash[:min_likes_count]
        ###puts "Text: #{text}"
        ###puts 'URL: ' + url
        ###puts '------------------------------------------'
        #add to returning array
        parsed_array << {name: tweet.user.name, likes: likes, url: url}
        #send to telegram chat
        #tlgrm_bot.send_message(chat_id: -225583588, text: "#{likes.to_s} likes tweet from Elon Musk\nLink: #{url}")
      else
        log "#{likes.to_s} > #{hash[:min_likes_count]}"
      end
    end
    return parsed_array
  end

  def person_exists?(screen_name)
    #binding.pry
    begin
      @client.user screen_name
      true
    rescue
      false
    end
  end

end