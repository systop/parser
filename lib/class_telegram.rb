require 'rubygems'
require "faraday"
require 'json'
require 'pry'
require 'pry-byebug'

class Telegram_api

  def initialize(config)
    @config = config
    check_token(@config)
  end

  def get_request(string)
    response = Faraday.new.get string
    body = response.body
    json = JSON.parse(body)
  end

  def check_token(config)
    req = "https://api.telegram.org/bot#{@config[:telegram_bot][:token]}/getMe"
    resp = get_request(req)
    unless resp['ok'] == true
      raise 'API bot Error'
    end
    log 'Telegram class created client'
  end

  def send_message(message)
    req = "https://api.telegram.org/bot#{@config[:telegram_bot][:token]}/sendMessage?chat_id=#{@config[:telegram_bot][:chat_id]}&text=#{message}"
    puts message
    #send to telegram chat
    resp = get_request(req)
    unless resp['ok'] == true
      raise 'API bot Error'
    end
    #@telegram_api.send_message(chat_id: -225583588, text: "#{message[:likes]} likes tweet from #{message[:name]}\nLink: #{message[:url]}")
  end

  def last_message_readed(response)
    reset_update_id = response['result'].last['update_id'] + 1
    req = "https://api.telegram.org/bot#{@config[:telegram_bot][:token]}/getUpdates?offset=#{reset_update_id}"
    response = get_request(req)
  end

  def listen(hash)
    #unpack variables
    tw_api = hash[:tw_api]
    config_path = hash[:config_path]

    req = "https://api.telegram.org/bot#{@config[:telegram_bot][:token]}/getUpdates"

    resp = get_request(req)
    #binding.pry
    unless resp['ok'] == true
      raise 'API bot Error' #поправить - выделить - может в лог
    end
    #check if exist new messages
    unless resp['result'].empty?
      #check that mess has text
      if resp['result'].last['message'].has_key? 'text'
      bot_mess = resp['result'].last['message']['text']
      puts "Новый запрос!---------------------\n#{bot_mess}"
        if bot_mess[0] == '/'
          #remove first symbol
          #bot_mess = bot_mess[1,bot_mess.length]
          bot_mess = remove_first_slash(bot_mess)
          case bot_mess
            #when '/hello' || "/hello@#{@config[:telegram_bot][:username]}"
            when /hello|hello@@config[:telegram_bot][:username]}/
              send_message("Дороу, #{resp['result'].last['message']['from']['first_name']} #{resp['result'].last['message']['from']['last_name']}!")
              send_info
              #send_message("Чтоб увидеть текущий конфиг, отправь '/show_config'\nДобавить персону в мониторинг: /add_person\nУдалить персону из конфига: /del_person")
            when /show_config|show_config@@config[:telegram_bot][:username]}/
              show_config
              send_info
            when /add_person|add_person@@config[:telegram_bot][:username]}/
            #when "/add_person"# or "/add_person@twitter_parser_systop_bot"
              #brifing
              send_message "Чтобы добавить в мониторинг новую персону, введи слеш, screen_name (@elonmusk только без собаки) и минимум лайков через запятую\nПример: `/biber,9000` (без кавычек)"
              #wait for new message
              #binding.pry
              mess_resp = wait_for_new_message(resp,req)
              messtext = mess_resp['result'].last['message']['text']
              puts messtext
              messtext = remove_first_slash(messtext)
              #check message
              if /^.+,\d+$/.match(messtext).nil?
                send_message 'неверная структура'
                raise 'wrong request structure'
              else
                send_message 'принято'
                mess_arr = messtext.split ','
      #binding.pry
                #check if name contained in config
                @config[:people].each do |person|
                  if person[:screen_name] == mess_arr[0]
                    send_message "#{mess_arr[0]} уже есть в конфиге"
                    raise 'just exist in config'
                  end
                end
                #check if name exist on Twitter portal
                if tw_api.person_exists?(mess_arr[0])
                  send_message "#{mess_arr[0]} существует на Twitter.com"
                else
                  send_message "#{mess_arr[0]} не найден на Twitter.com"
                  raise 'not found on portal'
                end
                #add to config file
                @config[:people].push({screen_name: mess_arr[0], min_likes_count: mess_arr[1].to_i})
                File.open(config_path, 'w') {|f| f.write @config.to_yaml }
                send_message "конфиг обновлен"
                show_config
                send_info
                last_message_readed(mess_resp)
              end
            when /del_person|del_person@@config[:telegram_bot][:username]}/
            #when '/del_person' || "/del_person@#{@config[:telegram_bot][:username]}"
              send_message "Напиши слэш и screen_name персоны для удаления из конфига\nПример: '/Nik_Vasilyev' (со слэшем и без кавычек)"
              #binding.pry
              mess_resp = wait_for_new_message(resp,req)
              messtext = mess_resp['result'].last['message']['text']
              messtext = remove_first_slash(messtext)
              #check message
              if /^[^,.]+$/.match(messtext).nil?
                send_message 'неверная структура'
                raise 'wrong request structure'
              else
                #check if exist in config
                per_id = ""
                @config[:people].each do |person|
                  if person[:screen_name] == messtext
                    per_id = @config[:people].index(person)
                    break
                  end
                end
                if per_id.to_s.empty?
                  send_message "#{messtext} НЕ найден в конфиге"
                  raise 'not found in config'
                else
                  @config[:people].delete_at(per_id)
                  File.open(config_path, 'w') {|f| f.write @config.to_yaml }
                  send_message "конфиг обновлен"
                  show_config
                  send_info
                  last_message_readed(mess_resp)
                end
              end
            else
              send_message "неизвестный запрос"
          end
        end
      end
        last_message_readed(resp)
    end
  rescue
    send_message "процедура сброшена"
    send_info
    last_message_readed(resp)
  end

  def remove_first_slash(bot_mess)
    bot_mess = bot_mess[1,bot_mess.length]
  end

  def show_config
    answer = ""
    @config[:people].each do |person|
      answer += @config[:people].index(person).to_s
      answer += " | #{person[:screen_name]}"
      answer += " | #{person[:min_likes_count]}"
      answer += "\n"
    end
    send_message answer
  end

  def wait_for_new_message(resp,req)
    #binding.pry
    resp = last_message_readed(resp)
    #wait for new message
    counter = 0
    while resp['result'].empty?
      puts resp.to_s
      resp = get_request(req)
      counter += 2
      if counter > 100
        send_message 'таймаут ожидания ответа истек'
        raise 'time is out'
      end
      sleep 1
    end
    #messtext = resp['result'].last['message']['text']
    #return messtext
    puts resp.to_s
    return resp
  end

  def send_info
    send_message("Чтоб увидеть текущий конфиг: /show_config\nДобавить персону в мониторинг: /add_person\nУдалить персону из конфига: /del_person")
  end

end

